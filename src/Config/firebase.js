import firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";

// Initialize Firebase
const firebaseConfig = {
        apiKey: "AIza--------------_-------------------",// COMPLETAR
        authDomain: "react------.firebaseapp.com", // COMPLETAR
        databaseURL: "https://-------.firebaseio.com",// COMPLETAR
        projectId: "------",// COMPLETAR
        storageBucket: "----------------------",// COMPLETAR
        messagingSenderId: "----------",// COMPLETAR
        appId: "1:---------------------------------------"// COMPLETAR
};
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
db.settings({
        timestampsInSnapshot: true
});
firebase.auth=firebase.auth();
firebase.db=db;

export default firebase;