import React, { Component }  from 'react';
//import {Link} from "react-router-dom";
import { withRouter } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col'

const styles = {
    cards:{
        width:'70%',
        margin:'10%',
        marginTop:'40px'
    },
    col:{
        padding:'5px'
    }
}

class ProductoDetalle extends Component{
    constructor(props){
        super(props)
        this.state={
            mensaje:""
        }
    }
    handleClick = ()=>{
        this.setState({
            mensaje:"Gracias por su compra"
        })
    }
    
    render(){
        return (
            <Col style={styles.col}>

            <Card>
            <Card.Header>{this.props.data.category}</Card.Header>
            <Card.Img variant="top" src={this.props.data.image} />
            <Card.Body>
                <Card.Title>{this.props.data.name}</Card.Title>
                
                <Card.Subtitle>
                    ${this.props.data.price}
                </Card.Subtitle>

                <Card.Text>
                {
                    !this.props.buttons &&
                        <Card.Text>
                            {this.props.data.description}
                        </Card.Text>
                }

                </Card.Text>



                <Button variant="primary"  onClick={this.handleClick}>Comprar</Button>
                {
                    this.props.buttons &&
                    <Button variant="outline-primary" onClick={()=>this.props.history.push('/producto/'+this.props.id)}>Ver Detalle</Button>
                    
                }
                <Card.Text>
                {this.state.mensaje}
                </Card.Text>

            </Card.Body>
            {
                !this.props.buttons &&
                    <Card.Footer className="text-muted">SKU: {this.props.data.sku}</Card.Footer>
            }

            </Card>
            </Col>


        )  
    }
}

export default withRouter(ProductoDetalle);