import React, {useState} from "react";
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import firebase from "./Config/firebase"
import { useHistory } from "react-router-dom"
import ButtonWithLoading from "./ButtonWithLoading";
import Input from "./Input";

const styles = {
    cards:{
        width:'70%',
        margin:'auto',
        marginTop:'40px'

    }
}

function Login(){
    const history = useHistory();
    const [form,setForm] = useState({email: '',password:''});
    const [spinner,setSpinner] = useState(false);
    const handleSubmit = (e)=>{
        console.log("HandleSubmit",form)
        setSpinner(true);
        firebase.auth.signInWithEmailAndPassword(form.email,form.password)
        .then(data=>{
            console.log("data",data)
            history.push("/")
            setSpinner(false);

        })
        .catch(err=>{
            console.log("error",err)
            setSpinner(false);
        })
        e.preventDefault();
    }
    const handleChange = (e)=>{
        const target = e.target;
        const value = target.value;
        const name = target.name;

        setForm({
            ...form,
            [name]:value
        })
    }


    return(
        <>
<Card style={styles.cards}>
  <Card.Body>
    <Card.Title>Ingrese</Card.Title>
    <Form onSubmit={handleSubmit}>

        <Input label="Email" type="email" placeholder="Ingrese su email" name="email" value={form.email} change={handleChange} />
        <Input label="Contraseña" type="password" placeholder="Ingrese su contraseña" name="password" value={form.password} change={handleChange} />

        <ButtonWithLoading text="Ingresar" loading={spinner}/>
    
    </Form>

  </Card.Body>
</Card>

        </>
    )
}
export default Login