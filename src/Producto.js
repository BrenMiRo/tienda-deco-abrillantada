import React,{useState,useEffect} from "react";
import ProductoDetalle from "./ProductoDetalle";
import firebase from "./Config/firebase";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import SpinnerGeneral from "./SpinnerGeneral"



function Producto (props){

    const [producto,setProducto] = useState({})
    const [loading,setLoading] = useState(true);

    useEffect(
        () => {

firebase.db
        .collection("productos")
        .doc(props.match.params.id)
        .get()
        .then((doc) => {
        setProducto(doc);
        setLoading(false);
    });
  }, [props.match.params.id]);


    if (loading) {
        return(
            <SpinnerGeneral />
    )
    }else{
        return(
            <Container>
            <Row className="justify-content-md-center" xs={2} md={3} lg={4} sg={5}>

            <ProductoDetalle data={producto.data()} buttons={false}/>

            </Row>
            </Container>
        )
    }
}
export default Producto